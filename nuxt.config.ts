import path from 'path'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  alias: {
    $: path.resolve(__dirname, 'pages-modules')
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use "@/assets/css/_colors.scss" as *;`
        }
      }
    },
    plugins: [
      createSvgIconsPlugin({
        iconDirs: [path.resolve(__dirname, 'icons')],
        symbolId: 'icon-[dir]-[name]'
      })
    ]
  },
  app: {
    pageTransition: { name: 'page', mode: 'out-in' },
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no',
      title: '飞智科技-企业数字化创新与转型解决方案提供商',
      link: [{ rel: 'canonical', href: 'http://www.flyzhi.com' }],
      meta: [
        {
          name: 'keywords',
          content: '数字化转型,软件定制,解决方案,信息化作业,产品咨询,飞智科技,软件开发'
        },
        {
          name: 'description',
          content:
            '飞智科技成立于2023年，是国内领先的创新性IT解决方案服务商，致力于通过咨询、产品设计及软件工程助力企业开启创新的互联网转型和信息化作业。飞智科技始终以客户为中心，结合技术的可能性和行业最佳实践，为全行业企业提供定制化数字解决方案。'
        }
      ]
    }
  },
  modules: ['@nuxtjs/tailwindcss']
})
