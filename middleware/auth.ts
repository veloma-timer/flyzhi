const isAuthenticated = () => true

export default defineNuxtRouteMiddleware((to, from) => {
  console.log('执行拦截')


  if (isAuthenticated() === false) {
    return navigateTo('/')
  }
})
