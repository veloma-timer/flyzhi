export default defineAppConfig({
  title: 'Flyzhi',
  theme: {
    dark: false,
    colors: {
      primary: '#FF6B01'
    }
  }
})
