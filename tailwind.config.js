/** @type {import('tailwindcss').Config} */
module.exports = {
  important: true,
  content: ['./pages-modules/**/*.vue'],
  theme: {
    extend: {},
    screens: {
      'min780px': '780px',
      'max780px': { max: '780px' }
    }
  },
  plugins: []
}
